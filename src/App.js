import React from "react";
import Home from "./Components/Home/Home";
import {BrowserRouter, Switch, Route, NavLink} from "react-router-dom";
import Add from "./Components/Add/Add";
import About from "./Components/About/About";
import Contacts from "./Components/Contacts/Contacts";
import ReadMore from "./Components/ReadMore/ReadMore";
import Edit from "./Components/Edit/Edit";
import './App.css';


function App() {
  return (
    <div className="App">
      <div className="top"/>
      <div className="blogNav">
        <h1 className="mainTitle">My Blog</h1>
        <BrowserRouter>
            <ul className="ul">
                <li><NavLink to="/" exact activeStyle={{color:'steelblue'}}>Home</NavLink></li>
                <li><NavLink to="/add" activeStyle={{color:'steelblue'}}>Add</NavLink></li>
                <li><NavLink to="/about" activeStyle={{color:'steelblue'}}>About</NavLink></li>
                <li><NavLink to="/contacts" activeStyle={{color:'steelblue'}}>Contacts</NavLink></li>
            </ul>
            <div className="switch">
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/readMore/:id" component={ReadMore}/>
                    <Route path="/edit/:id" component={Edit}/>
                    <Route path="/add" component={Add}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contacts" component={Contacts}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </div>

        </BrowserRouter>
      </div>

    </div>
  );
}

export default App;
