import React from 'react';
import axios from "axios";
import {useState, useEffect} from "react";

const ReadMore = ({history, match}) => {

    const [post, setPost] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(`https://my-project-app-number-1-default-rtdb.firebaseio.com/post/${match.params.id}.json`);
            setPost(response.data);
        }

        fetchData().catch(e => console.error(e));

    },[match.params.id]);


    const editPost = async e => {

        e.preventDefault();
        history.replace('/edit/' + match.params.id);
    }


    const deletePost = async e => {
        e.preventDefault();
        await axios.delete(`https://my-project-app-number-1-default-rtdb.firebaseio.com/post/${match.params.id}.json`);
        setPost('');
        history.replace('/');
    }

    return  post && (
        <>
            {Object.keys(post).map(kye => (
                    <div key={kye.toString()}>
                        <p>Title : {post.post.title}</p>
                        <p>Description : {post.post.description}</p>
                        <p>Date : {post.post.date}</p>
                    </div>
                )
            )}
            <button onClick={deletePost}>Delete</button>
            <button onClick={editPost}>Edit</button>
        </>
    );
};

export default ReadMore;