import React, {useEffect} from 'react';
import {useState} from "react";
import axios from "axios";
import './Home.css';

const Home = ({history}) => {

    const [getPosts, setGetPosts] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(`https://my-project-app-number-1-default-rtdb.firebaseio.com/post.json`);
            const rest = response.data
            setGetPosts(rest);
        }

        fetchData().catch(e => console.error(e));
    },[]);


    return getPosts && (
        <div>
            {Object.keys(getPosts).map(newP => (
                <div className="post" key={newP.toString()}>
                    <p>Date : {getPosts[newP].post.date}</p>
                    <p>Title : {getPosts[newP].post.title}</p>
                    <button className="btn" onClick={() => history.push("/readMore/" + newP)}>Read more</button>
                </div>
            ))}
        </div>
    );
}

export default Home;