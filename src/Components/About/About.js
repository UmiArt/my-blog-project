import React from 'react';
import './About.css';

const About = () => {
    return (
        <div className="about">
            <h1>About Us</h1>
            <h3>The problem with most About Us pages is that they’re an afterthought—a
            link buried at the bottom of the page that leads to a few hastily written paragraphs about a company.

            What an About Us page should be is a goal-oriented sales page, one that
            focuses on highlighting the biggest selling points of your story and brand, making a strong first
            impression on curious customers

            In Shopify’s customer trust research, we found that shoppers navigate to an About Us
            page to learn more about the brand and the people behind the products. Your About Page should address
                those two curiosities shoppers have to help with decision making.</h3>

        </div>
    );
};

export default About;