import React from 'react';
import './Add.css';
import {useState} from "react";
import axiosApi from "../../axiosApi";
import Spinner from "../../UI/Spinner/Spinner";
import dayjs from "dayjs";

const Add = ({history}) => {

    const [post, setPost] = useState({
        title: '',
        description: '',
        date: dayjs().format('DD/MM/YYYY HH:mm:ss'),
    });

    const [loading, setLoading] = useState(false);


    const onInputChange = e => {

        const {name, value} = e.target;

        setPost(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createPost = async e => {
        e.preventDefault();
        setLoading(true);

        try {
           await axiosApi.post('/post.json', {
               post
           })
        } finally {
            setLoading(false);
            history.replace('/')
        }
    };

    let form = (
        <form className='form' onSubmit={createPost}>Title
            <input className="Input"
                   type="text"
                   name="title"
                   value={post.title}
                   onChange={onInputChange}/>
            Description
            <textarea className='textarea'
                      name="description"
                      value={post.description}
                      onChange={onInputChange}/>
            <button>Save</button>
        </form>
    );

    if (loading) {
        form = <Spinner/>
    }


    return (
        <div className="add">
            <h3>Add new post</h3>
            {form}
        </div>
    );
};

export default Add;