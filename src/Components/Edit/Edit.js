import React from 'react';
import axios from "axios";
import {useState, useEffect} from "react";

const Edit = ({match, history, props}) => {

    const [editE, setEditE] = useState(null);

    useEffect(() => {

        const fetchData = async () => {
            const response = await axios.get(`https://my-project-app-number-1-default-rtdb.firebaseio.com/post/${match.params.id}.json`);
            setEditE(response.data);
        }

        fetchData().catch(e => console.error(e));

    },[match.params.id]);

    const onInputChange = (e) => {

        setEditE(prev => ({
            ...prev
        }))

    };

    const createPost = async e => {
        e.preventDefault();
        history.replace('/')

    };

    return editE && (
        <div>
            {Object.keys(editE).map(e => (
                    <div key={e.toString()}>
                        <form className='form' onSubmit={createPost}>Title
                            <input className="Input"
                                   type="text"
                                   name="title"
                                   value= {editE.post.title}
                                   onChange={onInputChange}/>
                            Description
                            <textarea className='textarea'
                                      name="description"
                                      value={editE.post.description}
                                      onChange={onInputChange}/>
                            <button>Save</button>
                        </form>
                    </div>
                )
            )}
        </div>
    );
};

export default Edit;