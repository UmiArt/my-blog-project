import React from 'react';
import './Contacts.css';

const Contacts = () => {
    return (
        <div className="contacts">
            <h1>Contact Us</h1>
            <h3>Have questions? We're happy to help.</h3>
            <h5 className="h5">The Virtuoso Service Center is open and available to assist you between 9 AM to 6
                PM Central Time (US), Monday through Friday.
            </h5>
            <h3>Tel :  +1-817-870-0300</h3>
            <h3>E-mail :  help@virtuoso.com</h3>

        </div>
    );
};

export default Contacts;